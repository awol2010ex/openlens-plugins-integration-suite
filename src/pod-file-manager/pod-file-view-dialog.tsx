import React from "react";
import { Renderer } from "@k8slens/extensions";
//自定义窗口样式
import "./pod-file-view-dialog.scss";

import { viewFileDialogState } from "./pod-file-manager-dialog";

const {
  Component: {
    Dialog,
    Wizard,
    WizardStep,
    Button,
  },
} = Renderer;
import { makeObservable } from "mobx";
import { observer } from "mobx-react";
import type { IComputedValue} from "mobx";
export interface PodFileViewDialogProps {
  isDialogOpen: IComputedValue<boolean>;
}

@observer
export class PodFileViewDialog extends React.Component<PodFileViewDialogProps> {
  constructor(props: PodFileViewDialogProps) {
    super(props);
    //接收变量的变化
    makeObservable(this);
  }

  //关闭菜单
  close() {
    viewFileDialogState.filecontent.set("");
    viewFileDialogState.isOpen.set(false);
  }

  render() {
    const {  ...dialogProps } = this.props;
    const  themeName = Renderer.Theme.activeTheme.get().name;
    const header = (
      <span>
        Pod File Manager( {viewFileDialogState.filepath.get()})
        <Button
          plain
          onClick={() => {
            viewFileDialogState.isOpen.set(false);
          }}
        >
          Close
        </Button>
      </span>
    );

    return (
      <Dialog
        {...dialogProps}
        className={"PodFileViewDialog"+themeName}
        isOpen={this.props.isDialogOpen.get()}
        close={this.close}
      >
        <Wizard header={header} done={this.close}>
          <WizardStep
            disabledNext={true}
            customButtons={
              <div className="buttons flex gaps align-center justify-space-between">
                <Button
                  plain
                  onClick={() => {
                    viewFileDialogState.isOpen.set(false);
                  }}
                >
                  Close
                </Button>
              </div>
            }
          >
            <pre>{viewFileDialogState.filecontent.get()}</pre>
          </WizardStep>
        </Wizard>
      </Dialog>
    );
  }
}
