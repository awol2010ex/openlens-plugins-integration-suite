import { Common } from "@k8slens/extensions";
import { observable, makeObservable } from "mobx";

export type LLMPreferenceModel = {
  baseURL: string;
  model: string;
  apiKey: string;
  useK8sTool: boolean;
  roleDesc: string ;
  podLogMaxLength : number;
};
export class LLMPreferenceModelStore extends Common.Store
  .ExtensionStore<LLMPreferenceModel> {
  // Store properties
  @observable baseURL = "https://open.bigmodel.cn/api/paas/v4";
  @observable model = "glm-4-flash";
  @observable apiKey = "";
  @observable useK8sTool = false;
  @observable roleDesc = "解答k8s 运维和异常问题,以及给出如何处理问题的建议";
  @observable podLogMaxLength=4096;

  constructor() {
    super({
      // Store name
      configName: "llm-preference-store",
      // Store default property values
      defaults: {
        baseURL: "https://open.bigmodel.cn/api/paas/v4",
        model: "glm-4-flash",
        apiKey: "",
        useK8sTool: false,
        roleDesc : "解答k8s 运维和异常问题,以及给出如何处理问题的建议",
        podLogMaxLength : 4096,
      },
    });
    makeObservable(this);
  }

  fromStore({ baseURL, model, apiKey ,useK8sTool,podLogMaxLength }: LLMPreferenceModel): void {
    this.baseURL = baseURL;
    this.model = model;
    this.apiKey = apiKey;
    this.useK8sTool =useK8sTool;
    this.podLogMaxLength = podLogMaxLength;
  }

  toJSON(): LLMPreferenceModel {
    return {
      baseURL: this.baseURL,
      model: this.model,
      apiKey: this.apiKey,
      useK8sTool: this.useK8sTool,
      roleDesc: this.roleDesc,
      podLogMaxLength: this.podLogMaxLength,
    };
  }
}

export const llmPreferenceStore = new LLMPreferenceModelStore();
