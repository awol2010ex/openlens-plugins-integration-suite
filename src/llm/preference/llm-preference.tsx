import { Renderer } from "@k8slens/extensions";
import { observer } from "mobx-react";
import React from "react";
import { llmPreferenceStore } from "./llm-preference-store";

const {
  Component: { SubTitle, Input ,Switch },
} = Renderer;

@observer
export class LLMPreference extends React.Component {
  render() {
    const separatorStyle = {
      minWidth: "40px",
      minHeight: "40px",
    };

    return (
      <section>
        <SubTitle title="Openai Compatible Preference" />
        <div style={separatorStyle}></div>
        <SubTitle title="BaseURL" />
        <Input
          theme="round-black"
          type="text"
          value={llmPreferenceStore.baseURL}
          onChange={(v) => {
            llmPreferenceStore.baseURL = v;
          }}
        />
        <div style={separatorStyle}></div>
        <SubTitle title="Model" />
        <Input
          theme="round-black"
          type="text"
          value={llmPreferenceStore.model}
          onChange={(v) => {
            llmPreferenceStore.model = v;
          }}
        />
        <div style={separatorStyle}></div>
        <SubTitle title="ApiKey" />
        <Input
          theme="round-black"
          type="password"
          value={llmPreferenceStore.apiKey}
          onChange={(v) => {
            llmPreferenceStore.apiKey = v;
          }}
        />

        <div style={separatorStyle}></div>
        <SubTitle title="RoleDesc" />
        <Input
          theme="round-black"
          type="text"
          value={llmPreferenceStore.roleDesc}
          onChange={(v) => {
            llmPreferenceStore.roleDesc = v;
          }}
        />
        <div style={separatorStyle}></div>
        <Switch
          checked={llmPreferenceStore.useK8sTool}
          onChange={() => {
            llmPreferenceStore.useK8sTool = !llmPreferenceStore.useK8sTool;
          }}
        >
          use k8s function call tools
        </Switch>

        <div style={separatorStyle}></div>
        <SubTitle title="Pod log fetch Max Bytes" />
        <Input
          theme="round-black"
          type="number"
          value={llmPreferenceStore.podLogMaxLength.toString()}
          onChange={(v) => {
            llmPreferenceStore.podLogMaxLength = parseInt(v);
          }}
        />
      </section>
    );
  }
}

export class LLMPreferenceHint extends React.Component {
  render() {
    return <span></span>;
  }
}
