import { Renderer } from "@k8slens/extensions";
const {
  K8sApi: { podsApi},
} = Renderer;
import { KubeConfig, CoreV1Api } from "@kubernetes/client-node";
import { tool } from "@langchain/core/tools";
import { z } from "zod";

const getPodTool = tool(
  async ({ name, namespace }: { name: string; namespace: string }) :Promise<string> => {
    console.log("name=", name);
    console.log("namespace=", namespace);
    const pod = await podsApi.get({ name: name, namespace: namespace });
    console.log("pod=", pod);
    return JSON.stringify(pod);
  },
  {
    name: "get_pod",
    description: "A tool to Get the Pod Info by Pod Name And Namespace",
    schema: z.object({
      name: z.string().describe("The name of the Pod"),
      namespace: z.string().describe("The namespace of the Pod"),
    }),
    verbose: true,
  }
);

export { getPodTool };
