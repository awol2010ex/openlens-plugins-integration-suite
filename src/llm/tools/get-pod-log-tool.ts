import { Renderer } from "@k8slens/extensions";

import { tool } from "@langchain/core/tools";
import { z } from "zod";
const {
  Catalog: { CatalogEntityRegistry },
} = Renderer;
import { KubeConfig, CoreV1Api } from "@kubernetes/client-node";
import { llmPreferenceStore } from "../../llm/preference/llm-preference-store";

const getPodLogTool = tool(
  async ({ name, namespace }: { name: string; namespace: string }) :Promise<string> => {
    console.log("name=", name);
    console.log("namespace=", namespace);
    const kubeconfigPath = new CatalogEntityRegistry().activeEntity.spec
      .kubeconfigPath;
    const kc = new KubeConfig();
    kc.loadFromFile(kubeconfigPath);

    const k8sApi = kc.makeApiClient(CoreV1Api);
    const log =await k8sApi.readNamespacedPodLog(name, namespace,null,false,true,llmPreferenceStore.podLogMaxLength );
    return log.body;
  },
  {
    name: "get_pod_log",
    description: "A tool to Get the Pod log by Pod Name And Namespace",
    schema: z.object({
      name: z.string().describe("The name of the Pod"),
      namespace: z.string().describe("The namespace of the Pod"),
    }),
    verbose: true,
  }
);

export { getPodLogTool };
