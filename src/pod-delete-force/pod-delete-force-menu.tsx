import React from "react";
import { Renderer, Common } from "@k8slens/extensions";

type Pod = Renderer.K8sApi.Pod;

const {
  Component: { MenuItem, Icon,  ConfirmDialog },
  Catalog: { CatalogEntityRegistry },
} = Renderer;
const { Util, App } = Common;
import { execFile } from "child_process";

export class PodDeleteForceMenu extends React.Component<
  Renderer.Component.KubeObjectMenuProps<Pod>
> {
  deleteForceImpl(pod: Pod) {
    const kubeconfigPath = new CatalogEntityRegistry().activeEntity.spec
      .kubeconfigPath;
    //console.log(App.Preferences.getKubectlPath());
    const kubectlPath = App.Preferences.getKubectlPath() || "kubectl";

    execFile(
      kubectlPath,
      [
        "--kubeconfig",
        kubeconfigPath,
        "delete",
        "pod",
        "-n",
        pod.getNs(),
        pod.getName(),
        "--force",
        "--grace-period=0",
      ],
      {},
      (error, stdout, stderr) => {
        if (error) {
          console.log(stderr);
          alert(stderr);
        } else {
          alert("Deleted");
        }
      }
    );
  }
  deleteForce(pod: Pod) {
    ConfirmDialog.open({
      ok: () => this.deleteForceImpl(pod),
      labelOk: `Delete Pod ` + pod.getName(),
      message: (
        <p>
          {"Are you sure you want to delete pod "}
          <b>{pod.getName()}</b>?
        </p>
      ),
    });
  }
  render() {
    const { object, toolbar } = this.props;

    return (
      <MenuItem
        onClick={Util.prevDefault(() => {
          this.deleteForce(object);
        })}
      >
        <Icon
          material="delete"
          interactive={toolbar}
          tooltip={toolbar && "Delete Pod Force"}
        />
        <span className="title">Delete Pod Force</span>
      </MenuItem>
    );
  }
}
