openlens平时常用的插件功能的打包集合

包含功能:

1. node shell (lens有但openlens 没有的功能)

2. pod shell (lens有但openlens 没有的功能)

3. ingress和secret tls证书过期时间查看 (lens 没有的功能)

4. 多个pod查看日志功能 (lens 没有的功能)
 
   注:需要先安装 https://github.com/stern/stern 并加到PATH环境变量

5. pod文件管理器功能 (lens 没有的功能)

6. helm release导出功能（导出为原始chart文件+自定义values） (lens 没有的功能)

7.ingress 链路图展示+ 根据url搜索 ingress链路图功能  (lens 没有的功能)

8. 整合netpolicy插件功能 (lens 没有的功能)

9. nodemerics ,podmerics ，compoenentstatus展示 (lens 没有的功能)

【构建前准备】

#安装VS 2022，组件选node.js 、c++

#打开Developer PowerShell for VS 2022

#安装fnm ： winget install Schniz.fnm

#进入项目目录，举例：cd E:\gitclone\openlens-plugins-integration-suite

#执行命令： 

fnm env --use-on-cd | Out-String | Invoke-Expression

npm cache clean --force

npm install --save-dev rimraf


#解压项目目录下的 @k8slens相关.7z 压缩包文件

#替换压缩包文件（以下路径请按项目实际路径为准）

rm -f E:\gitclone\openlens-plugins-integration-suite\node_modules\@k8slens

mv E:\gitclone\@k8slens   E:\gitclone\openlens-plugins-integration-suite\node_modules\

#执行构建： .\build.bat


【安装组件】

1、打开openlens主界面

2、ctrl+shift+e

3、点击install 

4、安装openlens-plugins-integration-suite-x.x.xx.tgz

5、需将任务栏右下角系统托盘图标中的lens彻底退出，重启后生效
